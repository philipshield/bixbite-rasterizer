#include "Camera.h"

Camera::Camera()
: position(CAMERA_defaultposition), focallength(CAMERA_defaultfocallength),
dyaw(0), dpitch(0), droll(0)
{
	SetRotation(0, 0, 0);
}

Camera::Camera(vec3 pos, float focallength, float yaw, float pitch, float roll)
: position(pos), focallength(focallength), dyaw(yaw), dpitch(pitch), droll(roll)
{
	SetRotation(yaw, pitch, roll);
}

void Camera::Rotate(float alpha, float beta, float gamma)
{
	dyaw += alpha;
	dpitch += beta;
	droll += gamma;

	SetRotation(dyaw, dpitch, droll);
}

void Camera::Move(vec3 v)
{
	position += v;
}

void Camera::SetRotation(vec3 viewdirection)
{
	float x = viewdirection.x;
	float y = viewdirection.y;
	float z = viewdirection.z;

	float yaw, pitch, roll;

	yaw = (z != 0) ? atan(std::sqrt(pow(x, 2.0f) + pow(y, 2.0f) / z)) : 0;
	pitch = (x != 0 && y != 0) ? atan(x / (-y)) : 0;
	roll = 0;

	SetRotation(yaw, pitch, roll);
}

void Camera::LookAt(vec3 target)
{
	vec3 vec = normalize(target - position);
	cout << "viewing direction: (" << vec.x << "," << vec.y << "," << vec.z << ")" << endline;
	SetRotation(vec);
}



/* Set Total Rotation to yaw, pitch, roll

| cos(a)cos(b)    cos(a)sin(b)sin(y) - sin(a)cos(y)    cos(a)sin(b)cos(y) + sin(a)sin(y) |
R =	| sin(a)cos(b)    sin(a)sin(b)sin(y) + cos(a)cos(y)    sin(a)sin(b)cos(y) - cos(a)sin(y) |
|	  -sin(b)				cos(b)sin(y)						        cos(b)cos(y)			 |

= Yaw*Pitch*Roll

*/
void Camera::SetRotation(float yaw, float pitch, float roll)
{
	//angles
	dyaw = yaw;
	dpitch = pitch;
	droll = roll;

	//matrices
	SetYaw(yaw);
	SetPitch(pitch);
	SetRoll(roll);

	//total rotation
	R = Roll*Pitch*Yaw;
}

/* Counterclockwise rotation on the z-axis

| cos(a)  -sin(a)  0 |
Rz = | sin(a)   cos(a)  0 |
|  0       0		1 |

*/
void Camera::SetRoll(float alpha)
{
	Roll[0][0] = cos(alpha);
	Roll[0][1] = -sin(alpha);
	Roll[0][2] = 0;
	Roll[1][0] = sin(alpha);
	Roll[1][1] = cos(alpha);
	Roll[1][2] = 0;
	Roll[2][0] = 0;
	Roll[2][1] = 0;
	Roll[2][2] = 1;
}

/* Counterclockwise rotation on the y-axis

| cos(b)  0  sin(b) |
Ry = |   0     1    0	 |
|-sin(b)  0  cos(b) |

*/
void Camera::SetYaw(float beta)
{
	Yaw[0][0] = cos(beta);
	Yaw[0][1] = 0;
	Yaw[0][2] = sin(beta);
	Yaw[1][0] = 0;
	Yaw[1][1] = 1;
	Yaw[1][2] = 0;
	Yaw[2][0] = -sin(beta);
	Yaw[2][1] = 0;
	Yaw[2][2] = cos(beta);
}


/* Counterclockwise rotation on the x-axis

|  1     0       0 		|
Rx = |  0   cos(y)  -sin(y)	|
|  0   sin(y)  cos(y)  |

*/
void Camera::SetPitch(float gamma)
{
	Pitch[0][0] = 1;
	Pitch[0][1] = 0;
	Pitch[0][2] = 0;

	Pitch[1][0] = 0;
	Pitch[1][1] = cos(gamma);
	Pitch[1][2] = -sin(gamma);

	Pitch[2][0] = 0;
	Pitch[2][1] = sin(gamma);
	Pitch[2][2] = cos(gamma);
}

/*
*  Getters
*/

const mat3& Camera::GetRotation() const
{
	return R;
}

const mat3& Camera::GetYaw() const
{
	return  Yaw;
}

const mat3& Camera::GetPitch() const
{
	return Pitch;
}

const mat3& Camera::GetRoll() const
{
	return Roll;
}

const float Camera::GetYawAngle() const
{
	return dyaw;
}

const float Camera::GetPitchAngle() const
{
	return dpitch;
}

const float Camera::GetRollAngle() const
{
	return droll;
}


vec3 Camera::Forward() const
{
	return normalize(FORWARD * GetRotation());
}

vec3 Camera::Backward() const
{
	return -Forward();
}


vec3 Camera::Right() const
{
	return normalize(RIGHT * GetRotation());
}


vec3 Camera::Left() const
{
	return -Right();
}

vec3 Camera::Up() const
{
	return normalize(UP * GetRotation());
}



vec3 Camera::Down() const
{
	return -Up();
}