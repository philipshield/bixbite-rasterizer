/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 *
 */

#ifndef _BPIXEL_H
#define _BPIXEL_H

#include "BBCommon.h"
using namespace glm;

struct Pixel
{
public:

	ivec2 pos;
	float zinv;
	vec3 illumination;
	vec3 pos3d;

	Pixel()
		:pos(0), zinv(0), pos3d(0)
	{}

	Pixel(ivec2 pos, int zinv)
		: pos(pos), zinv(zinv)
	{}

	int x() const { return pos.x; }
	int y() const { return pos.y; }

	~Pixel()
	{}
};


#endif