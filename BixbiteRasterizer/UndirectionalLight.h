/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains class definitions for graphics related datastructures used in Bixbite
*
*/

#ifndef BB_LIGHT_H
#define BB_LIGHT_H

/*
* Includes
*/

#include"BBCommon.h"
#include "BixbiteOptions.h"
using namespace std;
using namespace glm;

struct UndirectionalLight
{

	vec3 position;
	vec3 color;
	float intensity;
	float inaccuracy;
	float KConst, KLinear, KQuad;

	UndirectionalLight(vec3 pos, vec3 col, float intensity, float inacc = DEFAULT_LIGHT_INACC)
		: position(pos), color(col), intensity(intensity), inaccuracy(inacc),
		KConst(DEFAULT_CONST_ATTENUATION_COEFF),
		KLinear(DEFUALT_LINEAR_ATTENUATION_COEFF),
		KQuad(DEFUALT_QUADRATIC_ATTENUATION_COEFF)
	{
	}

	float Attenuation(float r)
	{
		return 1 / (KConst + KLinear*r + KQuad*r*r);
	}

};


#endif //BB_LIGHT_H