#include "SDLauxiliary.h"

SDL_Surface* InitializeSDL(int width, int height, bool fullscreen)
{
	atexit(SDL_Quit);
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		//Debug(Error, "could not initialize SDL"); exit(1);
	}
	Uint32 flags = SDL_SWSURFACE | SDL_ASYNCBLIT | SDL_DOUBLEBUF;
	return SDL_SetVideoMode(width, height, 32, flags);
}

bool NoQuitMessageSDL()
{
	SDL_Event e;
	while (SDL_PollEvent(&e))
	{
		//quit message
		if (e.type == SDL_QUIT)
			return false;

		//escape press
		if (e.type == SDL_KEYDOWN)
		{
			if (e.key.keysym.sym == SDLK_ESCAPE)
			{
				cout << "Pressed Escape: EXITING.." << endline;
				return false;
			}
		}
	}

	return true;
}
void PutPixelSDL(SDL_Surface* surface, int x, int y, vec3 color)
{
	if (x < 0 || surface->w <= x || y < 0 || surface->h <= y)
		return;

	Uint8 r = Uint8(glm::clamp(255 * color.r, 0.f, 255.f));
	Uint8 g = Uint8(glm::clamp(255 * color.g, 0.f, 255.f));
	Uint8 b = Uint8(glm::clamp(255 * color.b, 0.f, 255.f));

	Uint32* p = (Uint32*)surface->pixels + y*surface->pitch / 4 + x;
	*p = SDL_MapRGB(surface->format, r, g, b);
}
