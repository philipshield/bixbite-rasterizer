/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains the Bixbite top-level code.
* After bixbite is Instantiated and Initialized call Run().
* Run() is the bixbite main-loop
*
*/

#ifndef BIXBITE_VERTEX_H
#define BIXBITE_VERTEX_H

/*
* Includes, Imports
*/

#include "BBDebug.h"
#include "BBCommon.h"
#include "TestModel.h"

using namespace glm;
using namespace std;
using namespace std::chrono;


/*
 * Describes a Vertex
 */
struct Vertex
{
public:

	vec3 position;
	vec3 normal;
	vec3 reflectance;

	Vertex(vec3 pos, vec3 normal, vec3 reflectance) 
		: position(pos), normal(normal), reflectance(reflectance) {}

	Vertex() 
		: position(0), normal(0), reflectance(.4f) {}

	~Vertex() {}
};



#endif