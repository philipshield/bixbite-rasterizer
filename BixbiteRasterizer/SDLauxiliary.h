/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains boilerplate SDL wrapper functions
*
*/

#ifndef SDL_AUXILIARY_H
#define SDL_AUXILIARY_H

/*
* Includes & namespaces
*/

#include "BBCommon.h"

using namespace std;
using namespace glm;

/*
 * Typedefs
 */

/*
 * Globals 
 */

const vec3 COLOR_BLACK = vec3(0, 0, 0);
const vec3 COLOR_WHITE = vec3(255, 255, 255);

// Initializes SDL (video and timer). SDL creates a window where you can draw.
// A pointer to this SDL_Surface is returned. After calling this function
// you can use the function PutPixelSDL to do the actual drawing.
SDL_Surface* InitializeSDL(int width, int height, bool fullscreen = false);

/* 
 * Looks for events/messages sent to SDL, return true as no quit-event is received 
 */
bool NoQuitMessageSDL();

/* Draws a pixel on a SDL_Surface. 
 *
 * Before calling this function you should call:
 *	if( SDL_MUSTLOCK( surface ) )
 *		SDL_LockSurface( surface );
 *    
 * After calling this function you should call:
 *	if( SDL_MUSTLOCK( surface ) )
 *		SDL_UnlockSurface( surface );
 *	SDL_UpdateRect( surface, 0, 0, 0, 0 );
 */
void PutPixelSDL(SDL_Surface* surface, int x, int y, glm::vec3 color);

#endif