/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains the Bixbite top-level code.
* After bixbite is Instantiated and Initialized call Run().
* Run() is the bixbite main-loop
*
*/

#ifndef BIXBITE_MAIN_H
#define BIXBITE_MAIN_H

/*
* Includes, Imports
*/

#include "BBDebug.h"
#include "BBCommon.h"
#include "Camera.h"
#include "SDLauxiliary.h"
#include "TestModel.h"
#include "Pixel.h"
#include "Vertex.h"
#include "UndirectionalLight.h"

using namespace glm;
using namespace std;
using namespace std::chrono;

/*
* Typedefs
*/



/*
*  Main ``Game��-class.
*  Handles SDL setup, game-setup, draw/render-loop, unloading etc
*/
class Engine
{
private:

	SDL_Surface* screen = nullptr;
	Camera camera;

public:

	bool Initialized;

	/*
	* Bixbite constructor.
	* Creates a new bixbite object, using default values
	*/
	Engine();

	/*
	* Initializes SDL, Accelleration Structures, Scenes etc
	*/
	void Initialize();

	/*
	* Runs the rendering loop
	*/
	int Run();

	/*
	* Updates the scene
	*/
	void Update();

	/*
	* Renders the scene
	*/
	void Draw();

	/*
	 * Iterpolates a pixel with position, depth,..
	 */
	void Interpolate(Pixel a, Pixel b, vector<Pixel>& result);

	/*
	 * Draws a line
	 */
	void DrawLineSDL(SDL_Surface* surface, Pixel a, Pixel b, vec3 color);

	/*
	 * Draws only the edges of a polygon 
	 */
	void DrawPolygonEdges(const vector<Vertex>& vertices);

	/*
	 *
	 */
	void ComputePolygonRows(vector<Pixel>& vertexPixels, vector<Pixel>& leftPixels, vector<Pixel>& rightPixels); //TODO const vertexpix

	/*
	 * Draws precalculated polygons from right/left edges in screenspace (interpolating)
	 */
	void DrawPolygonRows(vector<Pixel>& leftPixels, vector<Pixel>& rightPolygons, vec3 color);



	/*
	* Translates world position to projection (screen) position.
	*/
	void VertexShader(const Vertex& worldPos, Pixel& pix);
	
	/*
	 * Called for each interpolated pixel, draws the pixel
	 */
	void PixelShader(const Pixel& pixel, vec3 color);

	/*
	 * Uppermost drawing function for polygon drawing
	 */
	void DrawPolygon(vector<Vertex>& polygon, vec3 color);

	/*
	 * calculates the direct light on a vertex from a lightsource
	 */
	vec3 DirectLight(const Vertex& vertex, UndirectionalLight& light);
	vec3 DirectLight(vec3 pos3d, vec3 norm, vec3 reflectance, UndirectionalLight& light);

	/*
	* Dummy method for testing stuff
	*/
	void TestCode();

	/*
	* Bixbite Destructor
	*/
	~Engine();
};



#endif
