/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
 * This file contains code for debugging purposes
 *
 */


#ifndef BBDEBUG_H
#define BBDEBUG_H

/*
 * Includes
 */

#include "BBCommon.h"


/*
 * Typedefs, Enums
 */

enum PrintType
{
	Trace = 0,
	Info,
	Warning,
	Error
};

struct PrintLabel
{
	PrintType _type;
	PrintLabel(PrintType type);
};

/*
 * Globals
 */

const PrintType DEBUG_LEVEL = PrintType::Info;// Debug messages must be at least this level to be printed.
const size_t INDENT_SIZE = 3;

namespace std
{
	ostream& operator<<(ostream& out, const PrintLabel& printLabel);
};

#ifdef DEBUG
#define Print(chain) do { cerr << chain; } while (0)
#define Debug(type, chain) do { if (type >= DEBUG_LEVEL) cerr << PrintLabel(type) << chain << endl; } while (0)
#define PrintLbl(type) do { cerr << PrintLabel(type); } while (0)
#define BreakExit() do { if (IsDebuggerPresent()) DebugBreak(); exit(0); } while (0) //<-- SET BREAKPOINT
#define DebugError(chain) do { cerr << PrintLabel(Error) << chain << endl; BreakExit(); } while (0)
#ifdef DEBUG
#define Assert(exp) assert(exp)
#else
#define Assert(exp) do { if (!(exp)) DebugError("Assertion failed"); } while (0) //<-- SET BREAKPOINT
#endif
#else
#define Print(chain)
#define Debug(type, chain)
#define PrintLbl(type)
#define DebugError(chain) do { exit(1); } while(0)
#define Assert(exp)
#endif

void PrintIndent();
void PrintUnindent();

struct AutoIndent
{
	AutoIndent()
	{
		PrintIndent();
	}
	~AutoIndent()
	{
		PrintUnindent();
	}
};


template <class Function>
__int64 time_call(Function&& f)
{
	__int64 begin = GetTickCount();
	f();
	return GetTickCount() - begin;
}


//
//+ Colouring
//

#ifdef DEBUG
enum CColor : int
{
	BLACK = 0,
	BLUE,
	GREEN,
	CYAN,
	RED,
	MAGENTA,
	BROWN,
	LIGHTGRY,
	DARKGRA,
	LIGHTBLE,
	LIGHTGREN,
	LIGHTCYN,
	LIGHTRE,
	LIGHTMAENT,
	YELLOW,
	WHITE
};

class Display {
public:
	Display();
	static void setTextColor(CColor color);
};
#endif







#endif