/* ** Bixbite Engine **
Version: 1.0

Contributor(s):
Philip Sk�ld	  <phisko@kth.se>
Anton Warnhag <awarnhag@kth.se>

END LICENSE BLOCK */

/*
* This file contains the main entry point
*
*/

/*
* Includes
*/
#include "Engine.h"
#include "BBCommon.h"
#include "SDLauxiliary.h"

using namespace glm;
using namespace std;

/*
* Main Entry Point
*/

Engine engine;

int main(int argc, char* argv[])
{
	engine.Initialize();
	int exitstatus = engine.Run();

	if (exitstatus != 0)
	{
		cout << "exiting with error code " << exitstatus << "." << endline;
	}
	else
	{
		cout << "exiting.." << endline;
	}
	return 0;
}