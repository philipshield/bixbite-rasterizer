﻿#include "Engine.h"

using namespace std;
using glm::vec3;
using glm::ivec2;
using glm::mat3;

// ----------------------------------------------------------------------------
// GLOBAL VARIABLES

//SDL, model, time etc
SDL_Surface* screen;
int t;

//rendering
vector<Triangle> triangles;
vector<vector<float>> zbuffer;

vec3 currN;		//for per_pixel_illumination
vec3 currRef;


//camera
vec3 cameraPos(0, 0, -4); // -3.001?
float focalLength = (SCREEN_WIDTH / 2);
Camera camera;

//light
UndirectionalLight light1(vec3(0, -0.8, 0), vec3(1, 1, 1), 10.f);
UndirectionalLight light2(vec3(0, -0.4, -0.8), vec3(1, 1, 1), 10.f);

//----------------------------------------------------------------------------

Engine::Engine()
: Initialized(false)
{
	Debug(Trace, "constructing Engine object");
}

void Engine::Initialize()
{
	Debug(Trace, "constructing Engine object");

	LoadTestModel(triangles);
	screen = InitializeSDL(SCREEN_WIDTH, SCREEN_HEIGHT);
	t = SDL_GetTicks();	// Set start value for timer.

	camera.focallength = focalLength;
	camera.position = cameraPos;

	//initialize zbuffer memory
	//depthBuffer = new float*[SCREEN_HEIGHT];
	//rep(i, 0, SCREEN_HEIGHT)
	//	depthBuffer[i] = new float[SCREEN_WIDTH];
	zbuffer = vector<vector<float>>(SCREEN_HEIGHT, vector<float>(SCREEN_WIDTH));

	Initialized = true;
	TestCode();
}

int Engine::Run()
{
	Debug(Trace, "Initializing Engine");
	if (!Initialized)
	{
		Debug(Error, "Engine not initialized.");
		return 1;
	}

	// UPDATE/DRAW LOOP
	while (NoQuitMessageSDL())
	{
		Update();

		if (SDL_MUSTLOCK(screen)) SDL_LockSurface(screen);

		Draw();

		if (SDL_MUSTLOCK(screen)) SDL_UnlockSurface(screen);
		SDL_UpdateRect(screen, 0, 0, 0, 0);
	}

	SDL_SaveBMP(screen, "screenshot.bmp");

	SDL_Delay(2000);

	//unload & quit
	SDL_Quit();
	return 0;
}

void Engine::Update()
{
	cerr << "."; //used to see the ticks
	Uint8* keystate = SDL_GetKeyState(0);
	float movedist = 0.09f;
	float alpha = 0.06f;

	//move light source
	if (keystate[SDLK_LSHIFT])
	{
		if (keystate[SDLK_UP])			{ light1.position += (movedist *  WORLD_UP); }
		if (keystate[SDLK_DOWN])			{ light1.position += (movedist * -WORLD_UP); }
		if (keystate[SDLK_RIGHT])		{ light1.position += (movedist * WORLD_RIGHT); }
		if (keystate[SDLK_LEFT])			{ light1.position += (movedist * -WORLD_RIGHT); }
		if (keystate[SDLK_PAGEUP])		{ light1.position += (movedist * WORLD_FORWARD); }
		if (keystate[SDLK_PAGEDOWN])		{ light1.position += (movedist * -WORLD_FORWARD); }
	}
	else
	{

		//camera 
		if (keystate[SDLK_0]){}
		if (keystate[SDLK_UP])		{ camera.Move(movedist*camera.Up()); }
		if (keystate[SDLK_DOWN])		{ camera.Move(movedist*camera.Down()); }
		if (keystate[SDLK_RIGHT])	{ camera.Move(movedist*camera.Right()); }
		if (keystate[SDLK_LEFT])		{ camera.Move(movedist*camera.Left()); }
		if (keystate[SDLK_w])		{ camera.Rotate(0, alpha, 0); }
		if (keystate[SDLK_s])		{ camera.Rotate(0, -alpha, 0); }
		if (keystate[SDLK_d])		{ camera.Rotate(alpha, 0, 0); }
		if (keystate[SDLK_a])		{ camera.Rotate(-alpha, 0, 0); }
		if (keystate[SDLK_q])		{ camera.Rotate(0, 0, -alpha); }
		if (keystate[SDLK_e])		{ camera.Rotate(0, 0, alpha); }
		if (keystate[SDLK_PAGEUP])	{ camera.Move(movedist * camera.Forward()); }
		if (keystate[SDLK_PAGEDOWN]){ camera.Move(movedist * camera.Backward()); }
		if (keystate[SDLK_p])
		{
			cout << endline;
			cout << "pos: <" << camera.position.x << ", " << camera.position.y << ", " << camera.position.z << ">" << endline;
			cout << "yaw = " << camera.GetYawAngle() << endline;
			cout << "pitch = " << camera.GetPitchAngle() << endline;
			cout << "roll = " << camera.GetRollAngle() << endline;
		}

		//if (keystate[SDLK_1]) { FirstScene.ChangeCamera(1); }
		//if (keystate[SDLK_2]) { FirstScene.ChangeCamera(2); }
		//if (keystate[SDLK_3]) { FirstScene.ChangeCamera(3); }
	}
	
}

void Engine::Draw()
{
	SDL_FillRect(screen, 0, 0);

	if (SDL_MUSTLOCK(screen))
		SDL_LockSurface(screen);

	vector<Vertex> vertices(3);
	rep(i, 0, triangles.size())
	{
		vertices[0] = Vertex(triangles[i].v0, triangles[i].normal, triangles[i].color);
		vertices[1] = Vertex(triangles[i].v1, triangles[i].normal, triangles[i].color);
		vertices[2] = Vertex(triangles[i].v2, triangles[i].normal, triangles[i].color);
		DrawPolygon(vertices, triangles[i].color);
	}

	//reset z-buffer
	//cout << zbuffer[249][249] << endline;
	//memset(depthBuffer, sizeof(depthBuffer), 0);
	for (int y = 0; y<SCREEN_HEIGHT; ++y)
		for (int x = 0; x<SCREEN_WIDTH; ++x)
			zbuffer[y][x] = 0;
	


	if (SDL_MUSTLOCK(screen))
		SDL_UnlockSurface(screen);

	SDL_UpdateRect(screen, 0, 0, 0, 0);
}





void Engine::DrawLineSDL(SDL_Surface* surface, Pixel a, Pixel b, vec3 color)
{
	ivec2 delta = glm::abs(a.pos - b.pos);
	int pixels = glm::max(delta.x, delta.y) + 1;

	vector<Pixel> line(pixels);
	Interpolate(a, b, line);
	for (int v = 0; v<line.size(); ++v)
	{
		PixelShader(line[v], line[v].illumination);
	}
}

void Engine::DrawPolygonEdges(const vector<Vertex>& vertices)
{
	int V = vertices.size();

	// Transform each vertex from 3D world position to 2D image position: 
	vector<Pixel> projectedVertices(V);
	for (int i = 0; i < V; ++i)
	{
		VertexShader(vertices[i], projectedVertices[i]);
	}

	// Loop over all vertices and draw the edge from it to the next vertex: 
	for (int i = 0; i<V; ++i)
	{
		int j = (i + 1) % V; // The next vertex 
		vec3 color(1, 1, 1);
		DrawLineSDL(screen, projectedVertices[i], projectedVertices[j], color);
	}
}

void Engine::ComputePolygonRows(vector<Pixel>& vertexPixels, vector<Pixel>& leftPixels, vector<Pixel>& rightPixels)
{
	//find topmost and lowermost pixel in vertexpixels
	int topY = SCREEN_HEIGHT, bottomY = 0, ydiff;
	trav(vp, vertexPixels)
	{
		topY =	  std::min(topY, (*vp).y());
		bottomY = std::max(bottomY, (*vp).y());
	}

	ydiff = bottomY - topY + 1; //how many pixel rows


	//interpolate to find edges and their depth
	leftPixels.resize(ydiff); rightPixels.resize(ydiff);
	rep(i, 0, ydiff)
	{
		leftPixels[i].pos.x =  +(numeric_limits<int>::max)();
		rightPixels[i].pos.x = -(numeric_limits<int>::max)();
	}

	rep(i, 0, vertexPixels.size())
	{
		//edge pixels
		int ia = i;
		int ib = (i + 1) % vertexPixels.size();
		Pixel a = vertexPixels[ia];
		Pixel b = vertexPixels[ib];

		//interpolate to find edge pixels and store in right/left
		ivec2 delta = glm::abs(a.pos - b.pos);
		vector<Pixel> edge(delta.y + 1);
		Interpolate(a, b, edge);
		trav(p, edge)
		{
			int curry = (*p).y() - topY;
			leftPixels[curry].pos.y =  (*p).y();
			rightPixels[curry].pos.y= (*p).y();

			if (leftPixels[curry].pos.x > (*p).x())
			{
				leftPixels[curry].pos.x = (*p).x();   //TODO: Unnecessary copies?
				leftPixels[curry].zinv = (*p).zinv;
				leftPixels[curry].illumination = (*p).illumination;
				leftPixels[curry].pos3d = (*p).pos3d;
			}

			if (rightPixels[curry].pos.x < (*p).x())
			{
				rightPixels[curry].pos.x = (*p).x();
				rightPixels[curry].zinv = (*p).zinv;
				rightPixels[curry].illumination = (*p).illumination;
				rightPixels[curry].pos3d = (*p).pos3d;
			}
		}
	}

}

void Engine::DrawPolygonRows(vector<Pixel>& leftPixels, vector<Pixel>& rightPixels, vec3 color)
{
	assert(leftPixels.size() == rightPixels.size()); //y-pixels
	rep(i, 0, leftPixels.size())
	{
		DrawLineSDL(screen, leftPixels[i], rightPixels[i], color);
	}
}

void Engine::Interpolate(Pixel a, Pixel b, vector<Pixel>& result)
{
	// interpolate position and zinv in N steps
	int N = result.size();
	float nsteps = float(std::max(N - 1, 1));
	vec2 step = vec2(b.pos - a.pos) / nsteps;
	float zstep = (b.zinv - a.zinv) / nsteps;

#ifdef PER_PIXEL_ILLUMINATION
	vec3 posstep = vec3(b.pos3d - a.pos3d) / nsteps;
	vec3 pos3d = a.pos3d;
#else
	vec3 ilstep = vec3(b.illumination - a.illumination) / nsteps;
	vec3 ill = a.illumination;
#endif

	vec2 current = a.pos;
	float zinv = a.zinv;
	for (int i = 0; i < N; ++i)
	{
		result[i].pos = current;
		current += step;

		result[i].zinv = zinv;
		zinv += zstep;
		assert(result[i].zinv >= 0 || result[i].zinv <= 1);
		
#ifdef PER_PIXEL_ILLUMINATION
		result[i].pos3d = pos3d;
		pos3d += posstep;
#else
		result[i].illumination = ill;
		ill += ilstep;
#endif

	}

	int viewinterp = 1;
}




vec3 Engine::DirectLight(const Vertex& vertex, UndirectionalLight& light)
{ 
	return DirectLight(vertex.position, vertex.normal, vertex.reflectance, light);
}

vec3 Engine::DirectLight(vec3 position, vec3 N, vec3 reflectance, UndirectionalLight& light)
{
	vec3 diffuse(0), specular(0);
	vec3 ambient = vec3(0);// vertex.reflectance * GLOBAL_AMBIENT;

	vec3 L((light.position - position /*+ sphericalRand(light.inaccuracy)*/));
	//if (!inShadow(scene, object, intersection, light)) // not shadow
	{
		//diffuse light
		float difflight = std::max(dot(N, L), 0.f);
		diffuse = reflectance * (light.color*light.intensity) * difflight;
		//cout << vertex.normal.x << " " << vertex.normal.y << " " << vertex.normal.z << endline;

		bool facing = dot(N, L) > 0;
		if (facing)
		{
			//specular
			vec3 V = normalize(position - camera.position);
			vec3 H = glm::reflect(-L, V);
			//specular = mat->specular * (light.color*light.intensity)
				//* (difflight > 0 ? std::pow(max(dot(N, H), 0), mat->shininess) : 0);
			specular = vec3(1) * (light.color*light.intensity)
				* (difflight > 0 ? std::pow(std::max(dot(N, H), 0.f), 80) : 0);
		}//*/

	}

	//composite light
	vec3 res = /*emissive +*/ ambient + light.Attenuation(length(L))*(diffuse);


	return res;
}

void Engine::VertexShader(const Vertex& vertex, Pixel& pix)
{
	// Transform to camera coordinates.
	vec3 trans_v = (vertex.position - camera.position) * camera.GetRotation();

	//projected pos
	pix.pos.x = round(camera.focallength * (trans_v.x / trans_v.z) + SCREEN_HEIGHT / 2);
	pix.pos.y = round(camera.focallength * (trans_v.y / trans_v.z) + SCREEN_HEIGHT / 2);

	//depth
	pix.zinv = 1.0 / length(vertex.position - camera.position);


	//illumination
#ifdef PER_PIXEL_ILLUMINATION
	pix.pos3d = vertex.position*pix.zinv; //interpolate p/z
#else
	pix.illumination = DirectLight(vertex, light1);
	//pix.illumination += DirectLight(vertex, light2);
#endif
}

void Engine::PixelShader(const Pixel& pixel, vec3 color)
{
	//current pixel
	int y = pixel.y() - 1, x = pixel.x() - 1;

	//bounds bailout
	if (y < 0 || x < 0 || y >= SCREEN_HEIGHT || x >= SCREEN_WIDTH) return;

	if (pixel.zinv > zbuffer[y][x])
	{
		zbuffer[y][x] = pixel.zinv;

#ifdef ZBUFF_COLOR
		PutPixelSDL(screen, x, y, vec3(pixel.zinv));
#else
#ifdef PER_PIXEL_ILLUMINATION
		PutPixelSDL(screen, x, y, DirectLight(pixel.pos3d/zbuffer[y][x], currN, currRef, light1));
#else
		PutPixelSDL(screen, x, y, color);
#endif
#endif

	}
}


//TODO: remove color from params
void Engine::DrawPolygon(vector<Vertex>& polygon, vec3 color)
{
	int V = polygon.size();
	vector<Pixel> vertexPixels(V);
	for (int i = 0; i < V; ++i)
		VertexShader(polygon[i], vertexPixels[i]);

	vector<Pixel> leftPixels, rightPixels;
	ComputePolygonRows(vertexPixels, leftPixels, rightPixels);	

	//do draw
#ifdef PER_PIXEL_ILLUMINATION
	currN = polygon[0].normal; //all same
	currRef = polygon[0].reflectance;
#endif

	DrawPolygonRows(leftPixels, rightPixels, color);

}


void Engine::TestCode()
{
	Debug(Trace, "running test code");

	cout << endline;
}


Engine::~Engine()
{
	//unload world
	Debug(Trace, "calling Engine Destructor");

}

